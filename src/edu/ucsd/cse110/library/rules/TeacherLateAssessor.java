package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class TeacherLateAssessor implements Assessor {

	@Override
	public boolean evaluate(Properties prop) {
		return prop.getType() == MemberType.Teacher 
				&& prop.getDays()>14;
	}

	@Override
	public String getErrors() {
		return null;
	}

}
